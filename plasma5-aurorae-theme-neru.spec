Name: plasma5-aurorae-theme-neru
Version: 1.0
Release: 1
Summary: 'Light and dark themes of the top bar for Arorae'
License: LGPL3
#Source0: https://github.com/chistota/plasma5-aurorae-theme-neru/archive/v1.0.tar.gz
%define __brp_mangle_shebangs %{nil}
%define debug_package %{nil}
%define srcdir %{_builddir}
%define pkgname plasma5-aurorae-theme-neru

%prep
 

%description
'Light and dark themes of the top bar for Arorae'

%install
    export pkgdir="%{buildroot}"
    export srcdir="%{srcdir}"
    export pkgname="%{pkgname}"
    export _pkgname="%{_pkgname}"
    export _pkgfolder="%{_pkgfolder}"
    export pkgver="%{version}"  
    cd "%{_builddir}"               
    tar -xzf v${pkgver}.tar.gz;
    cd "$srcdir"/"${pkgname}"-"$pkgver";
    install -d "$pkgdir/usr/share/aurorae/themes/NeruAuroraeLight";
    install -D -m644 NeruAurоrаеLight/*.svg "${pkgdir}"/usr/share/aurorae/themes/NeruAuroraeLight/;
    install -D -m644 NeruAurоrаеLight/metadata.desktop "${pkgdir}"/usr/share/aurorae/themes/NeruAuroraeLight/metadata.desktop;
    install -D -m644 NeruAurоrаеLight/NeruAurоrаеLightrc "${pkgdir}"/usr/share/aurorae/themes/NeruAuroraeLight/NeruAuroraeLightrc;
    cp -r NeruAuroraeDark "${pkgdir}/usr/share/aurorae/themes/NeruAuroraeDark";
    install -D -m644 README.md "${pkgdir}"/usr/share/doc/"${pkgname}"/README.md;
    install -D -m644 AUTHORS "${pkgdir}"/usr/share/doc/"${pkgname}"/AUTHORS;
    sed -i 's!Светлая тема верхней панели Неру v1.0!Neru Aurorae Light v1.0!' $pkgdir/usr/share/aurorae/themes/NeruAuroraeLight/metadata.desktop;
    sed -i 's!Темная тема верхней панели Неру v1.0!Neru Aurorae Dark v1.0!' $pkgdir/usr/share/aurorae/themes/NeruAuroraeDark/metadata.desktop;
    sed -i 's!Неру-светлая!Neru-Light!' $pkgdir/usr/share/aurorae/themes/NeruAuroraeLight/metadata.desktop;
    sed -i 's!Неру-темная!Neru-Dark!' $pkgdir/usr/share/aurorae/themes/NeruAuroraeDark/metadata.desktop

%files
/*
%exclude %dir /usr/bin
%exclude %dir /usr/lib

